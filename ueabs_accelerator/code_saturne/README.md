Installation Code_Saturne for KNLs
===================================

- Get the archive of Code_Saturne (4.2.2), and untar/gunzip the file into Code_Saturne_INSTALL_KNL
- cd Code_Saturne_INSTALL_KNL/SATURNE_4.2.2
- 2 files exists, code_saturne-4.2.2 and InstallHPC.sh
- Type ./InstallHPC.sh

The code is installed and code_saturne is to be found under code_saturne-4.2.2/arch/Linux/bin

Installation Code_Saturne and PETSc for GPUs
===================================

- Get the archive of Code_Saturne (4.2.2) prepared for PETSc (developer's version) and untar/gunsip the file into Code_Saturne_INSTALL_GPU
- cd Code_Saturne_INSTALL_GPU, where
- lapack-3.6.1 and sowing-1.1.23-pl have to be installed
- get cusp and put it under Code_Saturne_INSTALL_GPU
- install petsc using lapack-3.6.1 and sowing-1.1.23-pl and cusp
- go to SATURNE_4.2.2 and type ./InstallHPC.sh

The code is installed and code_saturne is to be found under code_saturne-4.2.2/arch/Linux/bin