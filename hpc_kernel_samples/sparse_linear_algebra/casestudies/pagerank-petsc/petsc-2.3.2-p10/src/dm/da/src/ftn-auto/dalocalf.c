#include "petsc.h"
#include "petscfix.h"
/* dalocal.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscda.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define dacreatelocalvector_ DACREATELOCALVECTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define dacreatelocalvector_ dacreatelocalvector
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define dagetlocalvector_ DAGETLOCALVECTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define dagetlocalvector_ dagetlocalvector
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define darestorelocalvector_ DARESTORELOCALVECTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define darestorelocalvector_ darestorelocalvector
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define dagetglobalvector_ DAGETGLOBALVECTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define dagetglobalvector_ dagetglobalvector
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define darestoreglobalvector_ DARESTOREGLOBALVECTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define darestoreglobalvector_ darestoreglobalvector
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   dacreatelocalvector_(DA da,Vec* g, int *__ierr ){
*__ierr = DACreateLocalVector(
	(DA)PetscToPointer((da) ),g);
}
void PETSC_STDCALL   dagetlocalvector_(DA da,Vec* g, int *__ierr ){
*__ierr = DAGetLocalVector(
	(DA)PetscToPointer((da) ),g);
}
void PETSC_STDCALL   darestorelocalvector_(DA da,Vec* g, int *__ierr ){
*__ierr = DARestoreLocalVector(
	(DA)PetscToPointer((da) ),g);
}
void PETSC_STDCALL   dagetglobalvector_(DA da,Vec* g, int *__ierr ){
*__ierr = DAGetGlobalVector(
	(DA)PetscToPointer((da) ),g);
}
void PETSC_STDCALL   darestoreglobalvector_(DA da,Vec* g, int *__ierr ){
*__ierr = DARestoreGlobalVector(
	(DA)PetscToPointer((da) ),g);
}
#if defined(__cplusplus)
}
#endif
