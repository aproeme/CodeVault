#ifndef MESH_H
#define MESH_H

#include <mpi.h>
#include <stdio.h>

#include "box.h"
#include "configuration.h"

typedef struct
{
   box_t domain;
   int *cell_indices;

   MPI_Datatype transfer_type_int;
   MPI_Datatype transfer_type_double;
} halo_info;

typedef struct
{
   int mpi_rank;
   halo_info halo_incoming;
   halo_info halo_outgoing;
} neighbor_info;

typedef struct
{
   box_t local_domain;
   box_t extended_local_domain; // with halo-cells

   int n_neighbors;
   neighbor_info *neighbors;

   MPI_Comm communicator;
} mesh_t;

void mesh_init(mesh_t *mesh, const conf_t *configuration, const box_t *decomposition);

void mesh_free(mesh_t *mesh);

int mesh_idx(const mesh_t *mesh, int x, int y, int z);

#endif

