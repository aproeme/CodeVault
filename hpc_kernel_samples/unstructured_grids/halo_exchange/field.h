#ifndef FIELD_H
#define FIELD_H

#include "configuration.h"
#include "mesh.h"

typedef struct
{
   const conf_t *configuration;
   const mesh_t *mesh;

   int *data;
} int_field_t;

void int_field_init(int_field_t *field, const conf_t *configuration, const mesh_t *mesh);

void int_field_free(int_field_t *field);

void int_field_set_constant(int_field_t *field, int value);

int int_field_check_value(const int_field_t *field, const box_t *domain, int expected_value);

void int_field_halo_exchange(int_field_t *field);

#endif
