#pragma once

#include "Communicator.hpp"

struct CollectiveCommunicator : public Communicator {
	using Communicator::Communicator;

	void Communicate(State* model) override;
	MpiRequest AsyncCommunicate(State* model) override;
};
