#include "P2PCommunicator.hpp"

void P2PCommunicator::Communicate(State* model) {
	AsyncCommunicate(model).Wait();
}

MpiRequest P2PCommunicator::AsyncCommunicate(State* model) {
	MpiRequest::DoubleVector<MPI_Request> reqs;
	for (std::size_t i{0}; i < neighbors_.size(); ++i) {
		{
			MPI_Request req;
			MPI_Isend(model + sendDisplacements_[i], // buf
			          1,                             // count
			          sendTypes_[i],                 // datatype
			          neighbors_[i],                 // dest
			          0,                             // tag
			          MPI_COMM_WORLD,                // comm
			          &req);                         // request
			reqs.push_back(req);
		}

		{
			MPI_Request req;
			MPI_Irecv(model + recvDisplacements_[i], // buf
			          1,                             // count
			          recvTypes_[i],                 // datatype
			          neighbors_[i],                 // source
			          0,                             // tag
			          MPI_COMM_WORLD,                // comm
			          &req);                         // request
			reqs.push_back(req);
		}
	}
	return MpiRequest{reqs};
}
