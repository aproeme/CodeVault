#ifndef READ_WRITE_LOCK
#define READ_WRITE_LOCK

#include <pthread.h>

namespace nbody {
	class ReadWriteLock {
	protected:
		pthread_mutex_t mutex;
		pthread_rwlock_t rwlock;
		pthread_t owner;
		bool owned;
	public:
		ReadWriteLock();
		virtual ~ReadWriteLock();
		virtual bool readLock();
		virtual bool writeLock();
		virtual bool upgradeToWriteLock();
		virtual bool downgradeToReadLock();
		virtual bool unLock();
	};
}

#endif
