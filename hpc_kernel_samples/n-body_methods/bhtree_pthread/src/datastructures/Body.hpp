#ifndef BODY_HPP
#define BODY_HPP

#include <vector>
#include <string>
#include <array>

namespace nbody {
	using namespace std;

	static const double timestep = 1.0;

	typedef struct DerivativeStruct {
		array<double, 3> dx;
		array<double, 3> dv;
	} Derivative;

	typedef struct BodyStruct {
		unsigned long id;
		array<double, 3> position;
		array<double, 3> velocity;
		array<double, 3> acceleration;
		double mass;
		int refinement;
	} Body;

	void resetAcceleration(Body& body);
	Derivative evaluate(Body body, double dt, Derivative d);
	void integrate(Body& body);
	void accumulateForceOntoBody(Body& to, Body from);
	bool validBody(Body body);
	void printBody(int parallelId, Body body);
	void printBodies(int parallelId, vector<Body> bodies);
	bool valid(vector<Body> bodies);
	vector<Body> dubinskiParse(string filename);
}

#endif
