#ifndef VECTOR_H
#define VECTOR_H

typedef struct
{
   double *x;
   double *y;
} vec_t;

void vec_init(vec_t *vector, int capacity);
void vec_reserve(vec_t *vector, int capacity);
void vec_free(vec_t *vector);
void vec_copy(vec_t *dst, int dst_start, const vec_t *src, int src_start, int count);

void vec_set_zero(vec_t *vector, int size);

void vec_add(vec_t *accumulator, const vec_t *operand, int size);
void vec_add_scaled(vec_t *accumulator, const vec_t* operand, double factor, int size);
void vec_add_scaled_mod(vec_t *accumulator, const vec_t* operand, double factor, double modulus_x, double modulus_y, int size);
void vec_scale(vec_t *vector, double factor, int size);

void vec_arr_divide(vec_t *result, const vec_t *operand, const double *divisor, int count);

#endif

