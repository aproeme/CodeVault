#ifndef MPITYPES_H
#define MPITYPES_H

#include <mpi.h>

#include "particles.h"

void mpitype_conf_init(MPI_Datatype *new_type);
void mpitype_conf_free(MPI_Datatype *type);

void mpitype_sim_info_init(MPI_Datatype *new_type);
void mpitype_sim_info_free(MPI_Datatype *type);

void mpitype_part_init_send(part_t *particles, MPI_Datatype *datatypes[], int *send_counts[], int *receiver_ranks[], int *n_receivers);
void mpitype_part_free_send(MPI_Datatype *datatypes[], int *send_counts[], int *receiver_ranks[], int n_receivers);

// returns the number of particles to be received
int mpitype_part_get_count(const MPI_Status *status);

void mpitype_part_init_recv(const part_t* particles, int blocklength, int displacement, MPI_Datatype* type);
void mpitype_part_free_recv(MPI_Datatype* type);

#endif

