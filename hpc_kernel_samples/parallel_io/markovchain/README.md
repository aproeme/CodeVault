# README - "Markov Chain": Analysis of a state sequence to extract history dependence


## Description

This example takes a state sequence from a specified file, and extracts patterns of state transitions.

## Contributors

 * Ahmet Tuncer Durak (UHeM / Turkey)

## Copyright

This code is available under Apache License, Version 2.0 - see also the license file in the CodeVault root directory.

## Languages

This sample is entirely written in C.

## Parallelisation

This sample uses MPI-2 for parallelisation.


## Level of the code sample complexity

Advanced


## Compiling

Follow the compilation instructions given in the main directory of the kernel samples directory (`/hpc_kernel_samples/README.md`).

## Running

To run the program, use something similar to:

    mpirun -n [nprocs] ./8_io_markovchain [inputfile]

Note that `inputfile` is simply treated as stream of characters representing different states.

